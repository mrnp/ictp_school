#ifndef NQS_VARIATIONAL_HH
#define NQS_VARIATIONAL_HH

#include <iostream>
#include <iomanip>
#include <Eigen/Dense>
#include <Eigen/IterativeLinearSolvers>
#include <random>
#include <complex>
#include <fstream>
#include <string>
#include <vector>
#include <mpi.h>

namespace nqs{

using namespace std;
using namespace Eigen;

template<class Hamiltonian,class RbmState,class Sampler,class Optimizer> class Variational{

  typedef Matrix<typename RbmState::StateType, Dynamic, 1 > VectorRbmT;
  typedef Matrix<typename RbmState::StateType, Dynamic, Dynamic > MatrixRbmT;

  Hamiltonian & ham_;
  Sampler & sampler_;
  RbmState & rbm_;

  VectorXd v_;
  vector<vector<int>> connectors_;
  vector<vector<double>> newconfs_;
  vector<double> mel_;

  VectorRbmT elocs_;
  MatrixRbmT Ok_;
  MatrixRbmT elocder_;
  VectorRbmT Okmean_;
  VectorRbmT Elocdermean_;

  MatrixXd vsamp_;
  MatrixRbmT S_;

  VectorXd grad_;

  typename RbmState::StateType elocmean_;
  int npar_;

  Optimizer & opt_;

  int Iter0_;

  bool use_sr_;
  double sr_diag_shift_;
  bool sr_rescale_shift_;

  bool use_newton_;
  double newton_diag_shift_;

  bool compute_eloc_der_;
  bool var_reduc_;

  int totalnodes_;
  int mynode_;

  ofstream filelog_;
  string filewfname_;
  double freqbackup_;


public:

  Variational(Hamiltonian & ham,Sampler & sampler,Optimizer & opt):
  ham_(ham),sampler_(sampler),rbm_(sampler.Rbm()),opt_(opt){

    npar_=rbm_.Npar();

    grad_.resize(npar_);
    opt_.SetNpar(npar_);
    Okmean_.resize(npar_);
    Elocdermean_.resize(npar_);

    Iter0_=0;
    use_sr_=false;
    use_newton_=false;
    compute_eloc_der_=false;
    var_reduc_=false;

    freqbackup_=0;

    MPI_Comm_size(MPI_COMM_WORLD, &totalnodes_);
    MPI_Comm_rank(MPI_COMM_WORLD, &mynode_);

    if(mynode_==0){
      cout<<"# Variational Monte Carlo running on "<<totalnodes_<<" processes"<<endl;
    }
  }

  void Sample(double nsweeps){
    sampler_.Reset();
    vsamp_.resize(nsweeps/totalnodes_,rbm_.Nvisible());

    for(double i=0;i<nsweeps/totalnodes_;i+=1){
      sampler_.Sweep();
      vsamp_.row(i)=sampler_.Visible();
    }

  }

  void SetVarReduc(bool var_reduc){
    var_reduc_=var_reduc;
    if(var_reduc_){
      use_sr_=false;
      compute_eloc_der_=true;
    }
  }

  //Sets the name of the files on which the logs and the wave-function parameters are saved
  //the wave-function is saved every freq steps
  void SetOutName(string filebase, double freq=100){
    filelog_.open(filebase+string(".log"));
    freqbackup_=freq;

    filewfname_=filebase+string(".wf");
  }

  bool Load(string filebase){
    string filename=filebase+".wf";

    ifstream filewf(filename);

    if(!filewf.is_open()){
      return false;
    }

    VectorXd part(npar_);
    for(int i=0;i<npar_;i++){
      if(!filewf.good()){
        return false;
      }
      filewf>>part(i);
    }

    filewf.close();

    rbm_.SetParameters(part);

    if(mynode_==0){
      cout<<"# Wave-function loaded from file "<<filename<<endl;
    }
    return true;
  }

  void Gradient(){

    const int nsamp=vsamp_.rows();
    elocs_.resize(nsamp);
    Ok_.resize(nsamp,rbm_.Npar());

    if(compute_eloc_der_){
      elocder_.resize(nsamp,rbm_.Npar());
      for(int i=0;i<nsamp;i++){
        elocder_.row(i)=DerEloc(vsamp_.row(i),elocs_(i));
        Ok_.row(i)=rbm_.DerLog(vsamp_.row(i));
      }
    }
    else{
      for(int i=0;i<nsamp;i++){
        elocs_(i)=Eloc(vsamp_.row(i));
        Ok_.row(i)=rbm_.DerLog(vsamp_.row(i));
      }
    }

    elocmean_=elocs_.mean();
    SumOnNodes(elocmean_);
    elocmean_/=double(totalnodes_);

    Okmean_=Ok_.colwise().mean();
    SumOnNodes(Okmean_);
    Okmean_/=double(totalnodes_);

    Ok_=Ok_.rowwise()-Okmean_.transpose();

    elocs_-=elocmean_*VectorXd::Ones(nsamp);

    grad_=VectorRbmT::Zero(npar_);

    if(compute_eloc_der_){
      Elocdermean_=elocder_.colwise().mean();
      SumOnNodes(Elocdermean_);
      Elocdermean_/=double(totalnodes_);

      elocder_=elocder_.rowwise()-Elocdermean_.transpose();

      if(var_reduc_){
        grad_+=elocder_*elocs_;
      }
    }

    if(!var_reduc_){
      grad_+=2.*(Ok_.adjoint()*elocs_).real();
    }

    //Summing the gradient over the nodes
    SumOnNodes(grad_);
    grad_/=double(totalnodes_*nsamp);

  }


  typename RbmState::StateType Eloc(const VectorXd & v){

    ham_.FindConn(v,mel_,connectors_,newconfs_);

    assert(connectors_.size()==mel_.size());

    auto logvaldiffs=(rbm_.LogValDiff(v,connectors_,newconfs_));

    assert(mel_.size()==logvaldiffs.size());

    typename RbmState::StateType eloc=0;

    for(int i=0;i<logvaldiffs.size();i++){
      eloc+=mel_[i]*std::exp(logvaldiffs(i));
    }

    return eloc;
  }

  //compute the derivative of the local energy on a given visible state
  //the local energy is also computed as a by-product
  VectorRbmT DerEloc(const VectorXd & v,typename RbmState::StateType & eloc){

    ham_.FindConn(v,mel_,connectors_,newconfs_);

    assert(connectors_.size()==mel_.size());

    auto logvaldiffs=rbm_.LogValDiff(v,connectors_,newconfs_);
    auto derlogdiffs=rbm_.DerLogDiff(v,connectors_,newconfs_);

    assert(mel_.size()==logvaldiffs.size());

    VectorRbmT der_eloc=VectorRbmT::Zero(npar_);

    eloc=0.;
    for(int i=0;i<logvaldiffs.size();i++){
      eloc+=mel_[i]*std::exp(logvaldiffs(i));
      der_eloc+=mel_[i]*std::exp(logvaldiffs(i))*derlogdiffs.row(i);
    }

    return der_eloc;
  }

  double ElocMean(){
    return real_part(elocmean_);
  }

  void Run(double nsweeps,double niter){
    opt_.Reset();

    for(double i=0;i<niter;i++){

      Sample(nsweeps);

      Gradient();

      UpdateParameters();

      PrintOutput(i);
    }
    Iter0_+=niter;
  }


  void UpdateParameters(){

    VectorXd pars=rbm_.GetParameters();

    if(use_sr_){
      SrStep();
    }
    if(use_newton_){
      NewtonStep();
    }

    opt_.Update(grad_,pars);

    SendToAll(pars);

    rbm_.SetParameters(pars);
  }

  void SrStep(){
    const int nsamp=vsamp_.rows();

    S_=Ok_.adjoint()*Ok_;
    SumOnNodes(S_);
    S_/=double(nsamp*totalnodes_);

    //Adding diagonal shift
    // S_+=MatrixXd::Identity(pars.size(),pars.size())*sr_diag_shift_;
    for(int i=0;i<npar_;i++){
      S_(i,i)+=sr_diag_shift_*S_(i,i);
    }

    VectorRbmT b=Ok_.adjoint()*elocs_;
    SumOnNodes(b);
    b/=double(nsamp*totalnodes_);

    auto sol=S_.colPivHouseholderQr();
    // sol.setThreshold(1.0e-5);
    VectorRbmT deltaP=sol.solve(b);


    grad_=deltaP.real();

    if(sr_rescale_shift_){
      complex<double> nor=(deltaP.dot(Ok_.adjoint()*Ok_*deltaP));
      grad_/=sqrt(nor.real());
    }
  }

  void NewtonStep(){
    const int nsamp=vsamp_.rows();

    if(var_reduc_){
      S_=2.*elocder_.adjoint()*elocder_;
    }
    else{
      // S_=Ok_.adjoint()*elocder_;
      // S_+=elocder_.adjoint()*Ok_;
      // S_+=2.*Ok_.adjoint()*(Ok_.colwise().cwiseProduct(elocs_)) ;
      cerr<<"Not implemented"<<endl;
    }

    SumOnNodes(S_);
    S_/=double(nsamp*totalnodes_);

    //Adding diagonal shift
    double eigmin=0;

    if(!var_reduc_){
      SelfAdjointEigenSolver<MatrixXd> es(S_,EigenvaluesOnly);
      eigmin = es.eigenvalues()[0];
      if(eigmin>0){
        eigmin=0;
      }
    }

    for(int i=0;i<npar_;i++){
      S_(i,i)+=newton_diag_shift_+eigmin;
    }

    VectorRbmT b=Ok_.adjoint()*elocs_;
    SumOnNodes(b);
    b/=double(nsamp*totalnodes_);

    VectorRbmT deltaP=S_.colPivHouseholderQr().solve(b);
    // VectorRbmT deltaP=S_.llt().solve(b);
    // auto sol=S_.jacobiSvd(ComputeThinU | ComputeThinV);
    // sol.setThreshold(1.0e-4);

    grad_=deltaP.real();
  }

  void PrintOutput(double i){
    auto Acceptance=sampler_.Acceptance();

    if(mynode_==0 && filelog_.is_open()){
      filelog_<<i+Iter0_<<"  "<<scientific<<std::setprecision(9)<<ElocMean()<<"   "<<grad_.norm()<<" "<<rbm_.GetParameters().array().abs().maxCoeff()<<" ";

      for(int a=0;a<Acceptance.size();a++){
        filelog_<<Acceptance(a)<<" ";
      }
      filelog_<<endl;
    }

    if(mynode_==0 && freqbackup_>0 &&  std::fmod(i,freqbackup_)<0.5){
      ofstream filewf(filewfname_);

      auto pars=rbm_.GetParameters();

      for(int p=0;p<pars.size();p++){
        filewf<<scientific<<std::setprecision(9)<<pars(p)<<endl;
      }

      filewf.close();
    }

  }


  void UseSr(bool usesr,double diagshift=0.01,bool rescale_shift=false){
    use_sr_=usesr;
    sr_diag_shift_=diagshift;
    sr_rescale_shift_=rescale_shift;

    if(use_sr_){
      compute_eloc_der_=false;
    }
  }

  void UseNewton(bool usenewton,double diagshift=0.1,bool varred=false){
    var_reduc_=varred;
    use_newton_=usenewton;
    newton_diag_shift_=diagshift;

    if(use_newton_){
      compute_eloc_der_=true;
    }
  }

  void ComputeElocDer(bool ceder){
    compute_eloc_der_=ceder;
  }

  //Debug function to check that the logarithm of the derivative is
  //computed correctly
  void CheckDerLog(double eps=1.0e-4){

    std::cout<<"# Debugging Derivatives of Wave-Function Logarithm"<<std::endl;
    std::flush(std::cout);

    sampler_.Reset(true);

    auto ders=rbm_.DerLog(sampler_.Visible());

    auto pars=rbm_.GetParameters();

    for(int i=0;i<npar_;i++){
      pars(i)+=eps;
      rbm_.SetParameters(pars);
      typename RbmState::StateType valp=rbm_.LogVal(sampler_.Visible());

      pars(i)-=2*eps;
      rbm_.SetParameters(pars);
      typename RbmState::StateType valm=rbm_.LogVal(sampler_.Visible());

      pars(i)+=eps;

      typename RbmState::StateType numder=(-valm+valp)/(eps*2);

      if(std::abs(numder-ders(i))>eps*eps){
        cerr<<" Possible error on parameter "<<i<<". Expected: "<<ders(i)<<" Found: "<<numder<<endl;
      }
    }

    std::cout<<"# Test completed"<<std::endl;
    std::flush(std::cout);
  }


  //Debug function to check that the derivative of the local energy is
  //computed correctly
  void CheckDerEloc(double eps=1.0e-4){

      std::cout<<"# Debugging Derivatives of Local Energy"<<std::endl;
      std::flush(std::cout);

      sampler_.Reset(true);

      typename RbmState::StateType eloc;
      auto ders=DerEloc(sampler_.Visible(),eloc);

      auto pars=rbm_.GetParameters();

      for(int i=0;i<npar_;i++){
        pars(i)+=eps;
        rbm_.SetParameters(pars);
        typename RbmState::StateType valp=Eloc(sampler_.Visible());

        pars(i)-=2*eps;
        rbm_.SetParameters(pars);
        typename RbmState::StateType valm=Eloc(sampler_.Visible());

        pars(i)+=eps;

        typename RbmState::StateType numder=(-valm+valp)/(eps*2);

        if(std::abs(numder-ders(i))>eps*eps){
          cerr<<" Possible error on parameter "<<i<<". Expected: "<<ders(i)<<" Found: "<<numder<<endl;
        }
      }

      std::cout<<"# Test completed"<<std::endl;
      std::flush(std::cout);
    }

  inline double real_part(double val)const{
    return val;
  }
  inline double real_part(const std::complex<double> & val)const{
    return val.real();
  }
};


}

#endif
