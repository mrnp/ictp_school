#ifndef NQS_MACHINES_HH
#define NQS_MACHINES_HH

namespace nqs{
  class RbmBinary;
  class RbmSpin;
}

#include "rbm_binary.hh"
#include "rbm_spin.hh"

#endif
