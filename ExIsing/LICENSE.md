############################ COPYRIGHT NOTICE ##################################  

Code provided by Giuseppe Carleo, ETH Zurich.  

Permission is granted for anyone to copy, use, modify, or distribute the  
accompanying programs and documents for any purpose, provided this copyright  
notice is retained and prominently displayed, along with a complete citation of  
papers:  

  
1. G. Carleo, and M. Troyer - Science 355, 602 (2017)    

2. G. Torlai et al. - arXiv:1703.05334 (2017)    
  
The programs and documents are distributed without any warranty, express or    
implied.  

These programs were written for research purposes only, and are meant to  
demonstrate and reproduce the main results obtained in the papers.  

All use of these programs is entirely at the user's own risk.  

################################################################################  
